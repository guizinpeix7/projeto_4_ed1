/* Universidade de Brasília - Faculdade do Gama
 * Estruturas de Dados 1 - 193704 TURMA: B
 * Professor: Mateus Mendelson
 * Nomes: Bruno Carmo Nunes 18/0117548
 *        Guilherme Dourado 16/0123020
 *        Guilherme Peixoto 17/0034941
 * Trabalho 4
*/


#include <stdio.h>
#include <stdlib.h>
#include <string.h> 
#define MAX_CD 99


typedef struct AVIOES_DECOLAR 
{
    char Codigo[MAX_CD];
    int prioridade;
    struct AVIOES_DECOLAR *next;
} Avioes_decolar;

typedef struct AVIOES_D_FILA{
    struct AVIOES_DECOLAR *front;  
    struct AVIOES_DECOLAR *end; 
}Apd;

Avioes_decolar *push_back_d(Apd *, char [MAX_CD]);
Apd *inicializa_fila_decol();
Avioes_decolar *pop_front_d(Apd *rm_front);
int tamanho_lista_d(Apd *frontRnode);

Apd *inicializa_fila_decol()
{
    Apd *ptr_aviao;
    ptr_aviao = (Apd *)malloc(sizeof(Apd));
    ptr_aviao->front = NULL;
    ptr_aviao->end = NULL;
    return ptr_aviao;
};

Avioes_decolar *push_back_d(Apd *ptr_lista, char Cod[MAX_CD])
{
    Avioes_decolar *meu_aviao;
    meu_aviao = (Avioes_decolar *)malloc(sizeof(Avioes_decolar));

    if (ptr_lista->front == NULL && ptr_lista->end == NULL)
        ptr_lista->front = ptr_lista->end = meu_aviao;
    if (ptr_lista->front == NULL)
        ptr_lista->front = ptr_lista->end = NULL;
    
    ptr_lista->end->next = meu_aviao;
    ptr_lista->end = meu_aviao;

    strcpy(meu_aviao->Codigo, Cod);
    meu_aviao->next = NULL;
    return meu_aviao;
}

Avioes_decolar *pop_front_d(Apd *rm_front)
{
    if (rm_front->front == NULL)
    {
        printf("A lista ja esta vazia\n");
        return NULL;
    }

    Avioes_decolar *meu_aviao;
    meu_aviao = rm_front->front;
    rm_front->front = rm_front->front->next;
    free(meu_aviao);
}void Imprime_lista_d(Apd *frontRnode)
{
    Avioes_decolar *meu_aviao;

    for (meu_aviao = frontRnode->front; meu_aviao != NULL; meu_aviao = meu_aviao->next)
    {
        printf("-> [%d] \n", meu_aviao->prioridade);
    }
}

int tamanho_lista_d(Apd *frontRnode){
    int i = 0;
    Avioes_decolar *meu_aviao; 
    for (meu_aviao = frontRnode->front; meu_aviao != NULL; meu_aviao = meu_aviao->next)
        i++;
    return i; 
}
