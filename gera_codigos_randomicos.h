/* Universidade de Brasília - Faculdade do Gama
 * Estruturas de Dados 1 - 193704 TURMA: B
 * Professor: Mateus Mendelson
 * Nomes: Bruno Carmo Nunes 18/0117548
 *        Guilherme Dourado 16/0123020
 *        Guilherme Peixoto 17/0034941
 * Trabalho 4
*/


#include <stdio.h>
#include <string.h> 
#include <stdlib.h> 

int *generate_r_comb(int);
int *generate_r_nmb_Voos();
char **gera_randon_N_cod_pousos(int);
char **gera_randon_N_cod_decol(int );

char **gera_randon_N_cod_pousos(int numero_de_pousos){

    char **codigos;
    codigos = (char **) malloc(numero_de_pousos*sizeof(char*));
    for(int i = 0 ; i < numero_de_pousos ; i++){
        codigos[i] = (char*) malloc(7*sizeof(char));
    }
    srand(time(NULL));
    
    for(int i = 0 ; i < numero_de_pousos ; i++){
        
        codigos[i][0] = rand() % (90-65+1)+65;
        codigos[i][1] = rand() % (90-65+1)+65;
        codigos[i][2] = rand()% (57-48+1)+48;
        codigos[i][3] = rand()%(57-48+1)+48;
        codigos[i][4] = rand()%(57-48+1)+48;
        codigos[i][5] = rand()%(57-48+1)+48;

        //printf("%s", codigos[i]); 
    } 
       return codigos; 
}

char **gera_randon_N_cod_decol(int numero_de_decol){

    char **codigos;
    codigos = (char **) malloc(numero_de_decol*sizeof(char*));
    for(int i = 0 ; i < numero_de_decol ; i++){
        codigos[i] = (char*) malloc(7*sizeof(char));
    }
  
    
    for(int i = 0 ; i < numero_de_decol ; i++){
        
        codigos[i][0] = rand() % (90-65+1)+65;
        codigos[i][1] = rand() % (90-65+1)+65;
        codigos[i][2] = rand()% (57-48+1)+48;
        codigos[i][3] = rand()%(57-48+1)+48;
        codigos[i][4] = rand()%(57-48+1)+48;
        codigos[i][5] = rand()%(57-48+1)+48;

        //printf("%s\n", codigos[i]); 
    } 
       return codigos; 
}

int *generate_r_nmb_Voos(){  //Numero de pousos
    int nVoos; 
    srand(time(NULL));
    nVoos = rand()%(64+1)+20;
    return nVoos; 
}
int *generate_r_nmb_Voos_decolagem(){ //Numero de decolagens
    int nVoos; 
    srand(time(NULL));
    nVoos = rand()%(32+1)+10;
    return nVoos; 
}
int *generate_r_comb(int numero_de_pousos){
    int *comb; comb = (int*) malloc(numero_de_pousos*sizeof(int));
    srand(time(NULL));
    for(int i = 0 ; i < numero_de_pousos ; i++){
        comb[i] = rand()%(12+1)+0;
    }
    return comb; 
}

