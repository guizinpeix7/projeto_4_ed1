/* Universidade de Brasília - Faculdade do Gama
 * Estruturas de Dados 1 - 193704 TURMA: B
 * Professor: Mateus Mendelson
 * Nomes: Bruno Carmo Nunes 18/0117548
 *        Guilherme Dourado 16/0123020
 *        Guilherme Peixoto 17/0034941
 * Trabalho 4
*/



#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <time.h>
#include <unistd.h>
#include "trab_4_Apouso.h"
#include "t4Avioes_decolar.h"
#include "gera_codigos_randomicos.h"
#define MAX_CD 99


void printa_tela(int min, Apf *minha_fila_pous, Apd *minha_fila_decol);
void Listagem(int min,char status, char *nome, int n_pist);

typedef struct PISTA_1
{
    int tempo_de_pouso;
    char Codigo[MAX_CD];
    bool uso;
} p1;

int main()
{
    Apf *reference_node_pouso;
    Apd *reference_node_decol;
    reference_node_pouso = inicializa_fila();
    reference_node_decol = inicializa_fila_decol();
    Avioes_pouso *my_p_aviao;
    Avioes_decolar *my_d_aviao;
    p1 pista_1;
    p1 pista_2;
    p1 pista_3;



///////////////////////////////RANDOMICO DE POUSOS////////////////
    int numero_de_pousos;
    numero_de_pousos = generate_r_nmb_Voos();
    //printf("%d\n", numero_de_pousos);
    
    srand(time(NULL));
    int *rand_comb; rand_comb = (int*) malloc(sizeof(int)); 
    rand_comb = generate_r_comb(numero_de_pousos);
    char **rand_COD;
    rand_COD = gera_randon_N_cod_pousos(numero_de_pousos); 
    for (int i = 0; i < numero_de_pousos; i++)
    {
        push_back(reference_node_pouso,rand_comb[i],rand_COD[i]);
    }
////////////////////////////////////RANDOMICO DE DECOLAGENS////////////
///*
////    int numero_de_decol;
////     numero_de_decol = generate_r_nmb_Voos_decolagem();// printf("OI ;D %d",generate_r_nmb_Voos_decolagem());
////    char **rand_COD_dec;
////    rand_COD_dec = gera_randon_N_cod_decol(numero_de_decol);
////  for (int i = 0; i < numero_de_decol; i++)
////    {
////        push_back_d(reference_node_pouso,rand_COD_dec[i]);
////    }
//*/
/////////////////////////////////////////////////////////////////////////
//    puts("Lista Inicial");
//    Imprime_lista(reference_node_pouso);
//    puts("Prioridades: ");

    int min = 0;
    pista_1.uso = false;
    char codigo1[100];
    char codigo2[100];
    char codigo3[100];
    pista_2.uso = false;

    printa_tela(0, reference_node_pouso, reference_node_decol);
    reference_node_pouso = prioridades(reference_node_pouso);

    usleep(2000000);



    system("clear");

    //A loucura começa aqui.

    while (1)
    {
        printa_tela(0, reference_node_pouso, reference_node_decol);

        if(reference_node_pouso->front == NULL){

            while(1){

                if(pista_1.uso == false && pista_2.uso == false && pista_3.uso == false) break;

                if(pista_1.uso == true && pista_1.tempo_de_pouso == 0){


                    Listagem((min-15),'P', codigo1, 1);
                    pista_1.uso = false;
                }

                if(pista_2.uso == true && pista_2.tempo_de_pouso == 0){

                    Listagem((min-15),'P', codigo2, 2);
                    pista_2.uso = false;
                }

                if(pista_3.uso == true && pista_3.tempo_de_pouso == 0){

                    Listagem((min-15),'P', codigo3, 3);
                    pista_3.uso = false;
                }

                usleep(5000000);

                system("clear");

            }

            break;
        }

        if(pista_1.uso == true && pista_1.tempo_de_pouso == 0){


           Listagem((min-15),'P', codigo1, 1);
           pista_1.uso = false;
        }

        if(pista_2.uso == true && pista_2.tempo_de_pouso == 0){

            Listagem((min-15),'P', codigo2, 2);
            pista_2.uso = false;
        }

        if(pista_3.uso == true && pista_3.tempo_de_pouso == 0){

            Listagem((min-15),'P', codigo3, 3);
            pista_3.uso = false;
        }

        if(Aciona_Emergencia(reference_node_pouso)){

            if(pista_1.uso == false) {
                strcpy(codigo3, reference_node_pouso->front->Codigo);
                pista_3.tempo_de_pouso = 15;
                pista_3.uso = true;

                my_p_aviao = pop_front(reference_node_pouso);

            }

        }

        if(pista_1.uso == false || pista_2.uso == false){

            if(pista_1.uso == false) {

                strcpy(codigo1, reference_node_pouso->front->Codigo);
                pista_1.tempo_de_pouso = 15;
                pista_1.uso = true;

            }
            else{

                strcpy(codigo2, (reference_node_pouso->front->Codigo));
                pista_2.tempo_de_pouso = 15;
                pista_2.uso = true;

            }
            my_p_aviao = pop_front(reference_node_pouso);

        }

        if(pista_1.uso == true){

            pista_1.tempo_de_pouso -= 5;

        }

        if(pista_2.uso == true){

            pista_2.tempo_de_pouso -= 5;

        }

        if(pista_3.uso == true){

            pista_3.tempo_de_pouso -= 5;

        }

        if( min != 0 && (min % 50) == 0){
            Decrementa(reference_node_pouso);
            reference_node_pouso = prioridades(reference_node_pouso);
        }

        min += 5;





        usleep(5000000);

        system("clear");

    }
}

void printa_tela(int min, Apf *minha_fila_pous, Apd *minha_fila_decol)
{
    int pouso = tamanho_lista(minha_fila_pous);
    int n_voos;
    Avioes_pouso *meu_aviao = minha_fila_pous->front;

    puts("---------------------------------------------------------------------------------");
    puts("“Aeroporto Internacional de Brasília”");
    printf("Hora Inicial: 8:0%d\n", min);
    printf("Fila de Pedidos: ");
        printf("[%s - P - %d] \n", meu_aviao->Codigo, meu_aviao->prioridade);
    n_voos = pouso;
    printf("NVoos: %d\n", n_voos);
    printf("Naproximações: %d\n", pouso);
    printf("NDecolagens: %d\n", 0);
    puts("---------------------------------------------------------------------------------");
    puts("Listagem de eventos:");


}
void Listagem(int min,char status, char *nome, int n_pist) {

    char status1[100] = "avião pousado\0";
    char status2[100] = "avião decolou\0";
    int hora = 8;

    hora += (min/60);

    printf("Código do voo: %s\n", nome);
    printf("Status: ");
    switch(status){

        case 'P' : printf("%s \n", status1); break;
        case 'D' : printf("%s \n", status2); break;

    }
    printf("Horário do início do procedimento: %d:%2d\n", hora,(min%60));
    printf("Número da pista: %d\n\n", n_pist);

}
