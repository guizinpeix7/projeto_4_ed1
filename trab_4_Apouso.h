/* Universidade de Brasília - Faculdade do Gama
 * Estruturas de Dados 1 - 193704 TURMA: B
 * Professor: Mateus Mendelson
 * Nomes: Bruno Carmo Nunes 18/0117548
 *        Guilherme Dourado 16/0123020
 *        Guilherme Peixoto 17/0034941
 * Trabalho 4
*/


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define MAX_CD 99

//Node (alternavel)
typedef struct AVIOES_POUSO
{
    char Codigo[MAX_CD];
    int prioridade;
    struct AVIOES_POUSO *next;
} Avioes_pouso;
/*typedef struct AVIOES_DECOLAR{
    char CODIGO[MAX_CD]; 
    struct AVIOES_DECOLAR *next; 
}Avioes_decolar;*/

//ESSE E FOGO... Aqui temos um tipo de dado(pensar num bloco) do tipo AVIOES_POUSO
//que cria um tipo de dado que aponta um pro comeco da fila e outro pro final... PREOLAAA!
//So que cria esses nodes tipo sem inserir nenhum conteudo dentro deles... e tipo um node estatico (fixo).
typedef struct AVIOES_POUSO_FILA
{
    struct AVIOES_POUSO *front;
    struct AVIOES_POUSO *end;
} Apf;

Apf *inicializa_fila();
int Vazia(Apf *);
Avioes_pouso *push_back(Apf *, int, char[MAX_CD]);
Avioes_pouso *pop_front(Apf *rm_front);
void Imprime_lista(Apf *);
Apf Decrementa(Apf *);
int Aciona_Emergencia(Apf *); 
int tamanho_lista(Apf *);
Apf *prioridades(Apf *Reference_node);


Apf *inicializa_fila()
{
    Apf *ptr_aviao;
    ptr_aviao = (Apf *)malloc(sizeof(Apf));
    ptr_aviao->front = NULL;
    ptr_aviao->end = NULL;
    return ptr_aviao;
};

//Essa funcao coloca os zeros no inicio da lista 
Apf *prioridades(Apf *Reference_node)
{
    Avioes_pouso *meu_aviao, *proximo_aviao;

    if (Vazia(Reference_node))
        return Reference_node; //Se a lista estiver vazia

    if (Reference_node->front->next == NULL)
    {                          //Se a lista nao estiver vazia mas so existirem 2 elementos
        return Reference_node; //Porque se retorno nulo da seg fault ?
    }

    meu_aviao = Reference_node->front; //Como alterei o endereco da minha primeira variavel devo atualiza-la de novo (:'
    proximo_aviao = Reference_node->front->next;



    //O for deve ser escrito desse jeito para que o for atualize proximo aviao antes do meu aviao, para que assim as variaveis nao sejam atualizadas de forma desordenada... Pulando alguns casos.
    for (; proximo_aviao->next != NULL; proximo_aviao = meu_aviao->next->next, meu_aviao = meu_aviao->next)
    {
        
        if (proximo_aviao->prioridade == 0)
        {
            meu_aviao->next = proximo_aviao->next;
            proximo_aviao->next = Reference_node->front;
            Reference_node->front = proximo_aviao;
        }
        proximo_aviao = meu_aviao->next->next;        
    }

    //Casa o ultimo node seja = 0; 
    if(proximo_aviao->next == NULL && proximo_aviao->prioridade == 0){
        puts("To entrando aqui"); 
        proximo_aviao->next = Reference_node->front; 
        Reference_node->front = meu_aviao->next;
        Reference_node->end = meu_aviao;
        meu_aviao->next = NULL; 
    }
    return Reference_node;
}

//Essa funcao coloca os zeros no final da lista 
/*Apf *prioridades(Apf *Reference_node){ 
    Avioes_pouso *meu_aviao,*proximo_aviao; 
    
    if(Vazia(Reference_node)) return Reference_node; //Se a lista estiver vazia 

    if(Reference_node->front->next == NULL){ //Se a lista nao estiver vazia mas so existirem 2 elementos
          return Reference_node; //Porque se retorno nulo da seg fault ?  
   }
           
    meu_aviao = Reference_node->front;
    
    //PARA CASO O PRIMEIRO NODE SEJA 0
    if(meu_aviao->prioridade == 0){
        Reference_node->end->next = meu_aviao; 
        Reference_node->front = meu_aviao->next; 
        Reference_node->end = meu_aviao; 
        meu_aviao->next = NULL; 
    }
    meu_aviao = Reference_node->front; //Como alterei o endereco da minha primeira variavel devo atualiza-la de novo (:'  
    proximo_aviao = Reference_node->front->next;

    //O for deve ser escrito desse jeito para que o for atualize proximo aviao antes do meu aviao, para que assim as variaveis nao sejam atualizadas de forma desordenada... Pulando alguns casos. 
    for( ; proximo_aviao->next != NULL ;proximo_aviao = meu_aviao->next->next, meu_aviao=meu_aviao->next){
        if(proximo_aviao->prioridade == 0 ){

            Reference_node->end->next = proximo_aviao; 
            meu_aviao->next = proximo_aviao->next;
            proximo_aviao->next = NULL;
            Reference_node->end = proximo_aviao;  
           // printf("Ta realmente funfando? ", proximo_aviao);
            
        }
        proximo_aviao = meu_aviao->next->next;
        //Imprime_lista(Reference_node); PARA DEBUGAR

    }
    //Imprime_lista(Reference_node);
    return Reference_node; 
}*/

//ptr_lista e o ponteiro que contem o inicio e o final da lista
Avioes_pouso *push_back(Apf *ptr_lista, int prioridade, char Cod[MAX_CD])
{
    Avioes_pouso *meu_aviao;
    meu_aviao = (Avioes_pouso *)malloc(sizeof(Avioes_pouso));

    if (ptr_lista->front == NULL && ptr_lista->end == NULL)
        ptr_lista->front = ptr_lista->end = meu_aviao;
    if (ptr_lista->front == NULL)
        ptr_lista->front = ptr_lista->end = NULL;

    ptr_lista->end->next = meu_aviao;
    ptr_lista->end = meu_aviao;

    strcpy(meu_aviao->Codigo, Cod);
    meu_aviao->prioridade = prioridade;
    meu_aviao->next = NULL;
    return meu_aviao;
}

Avioes_pouso *pop_front(Apf *rm_front) // tira do inicio da lista
{
    if (rm_front->front == NULL)
    {
        printf("A lista ja esta vazia\n");
        return NULL;
    }

    Avioes_pouso *meu_aviao;
    meu_aviao = rm_front->front;
    rm_front->front = rm_front->front->next;
    free(meu_aviao);
}

int Vazia(Apf *lista)
{
    if (lista->front == NULL)
        return 1;
    else
        return 0;
}

//Verifica se a lista esta vazia ou nao
void Imprime_lista(Apf *frontRnode)
{
    Avioes_pouso *meu_aviao;

    for (meu_aviao = frontRnode->front; meu_aviao != NULL; meu_aviao = meu_aviao->next)
    {
        printf("-> [%d] ", meu_aviao->prioridade);
    }
}

int Aciona_Emergencia(Apf *frontRnode){
    Avioes_pouso *aviao_1, *aviao_2, *aviao_3;
    if(frontRnode->front!=NULL && frontRnode->front->next != NULL && frontRnode->front->next->next != NULL){
        aviao_1 = frontRnode->front; 
        aviao_2 = frontRnode->front->next;
        aviao_3 = frontRnode->front->next->next;
        if(aviao_1->prioridade <= 0 && aviao_2->prioridade <= 0 && aviao_3->prioridade <= 0){
            puts("ALERTA GERAL DE DESVIO DE AERONAVE"); 
            return 1; 
        } 
        
    }
    return 0; 
    
}


int tamanho_lista(Apf *frontRnode){
    int i = 0;
    Avioes_pouso *meu_aviao; 
    for (meu_aviao = frontRnode->front; meu_aviao != NULL; meu_aviao = meu_aviao->next)
        i++;
    return i; 
}

Apf Decrementa(Apf *frontRnode)
{
    Avioes_pouso *meu_aviao;
    Aciona_Emergencia(frontRnode); 
    for (meu_aviao = frontRnode->front; meu_aviao != NULL; meu_aviao = meu_aviao->next)
    {
        meu_aviao->prioridade -= 1;
        if (meu_aviao->prioridade <= -1){
            pop_front(frontRnode);
            printf("O avião %s caiu, pessoas morreram, maldito Santos Dummont\n", meu_aviao->Codigo);
        }
    }
}


